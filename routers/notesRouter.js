const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('./middlewares/authMiddleware');
const {asyncWrapper} = require('./helpers');
const {createNote, getNotes, getNoteById, updateNote, checkNote, deleteNote} =
      require('../controllers/notesController');

router.post('/', authMiddleware, asyncWrapper(createNote));
router.get('/', authMiddleware, asyncWrapper(getNotes));
router.get('/:id', authMiddleware, asyncWrapper(getNoteById));
router.put('/:id', authMiddleware, asyncWrapper(updateNote));
router.patch('/:id', authMiddleware, asyncWrapper(checkNote));
router.delete('/:id', authMiddleware, asyncWrapper(deleteNote));

module.exports = router;
