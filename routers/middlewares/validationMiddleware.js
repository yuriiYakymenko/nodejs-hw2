const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .alphanum()
        .required(),

    password: Joi.string()
        .required(),
  });

  const validation = await schema.validate(req.body);
  const {error} = validation;
  console.log(error);
  if (error) {
    return res.status(400).json({
      message: error.message,
    });
  }

  next();
};
