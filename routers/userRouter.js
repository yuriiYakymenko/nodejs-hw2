const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('./middlewares/authMiddleware');
const {asyncWrapper} = require('./helpers');
const {getInfo, deleteUser, changePassword} =
        require('../controllers/userController');

router.get('/', authMiddleware, asyncWrapper(getInfo));
router.delete('/', authMiddleware, asyncWrapper(deleteUser));
router.patch('/', authMiddleware, asyncWrapper(changePassword));

module.exports = router;
