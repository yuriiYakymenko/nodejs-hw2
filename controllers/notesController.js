const {Note} = require('../models/noteModel');

module.exports.createNote = async (req, res) => {
  const {text} = req.body;
  const _id = req.user._id;

  if (!text) {
    return res.status(400).json({message: 'Enter valid text'});
  }

  const note = new Note({
    userId: _id,
    text: text,
  });

  await note.save();
  res.status(200).json({message: 'Success'});
};

module.exports.getNotes = async (req, res) => {
  const _id = req.user._id;
  const {offset, limit} = req.query;

  const notes = await Note.find({userId: _id})
      .skip(Number(offset))
      .limit(Number(limit));
  if (!notes) {
    return res.status(400).json({message: 'Notes were not found'});
  }

  res.status(200).json({notes});
};

module.exports.getNoteById = async (req, res) => {
  const _id = req.params.id;

  const note = await Note.findById(_id);
  if (!note) {
    return res.status(400).json({message: 'Note was not found'});
  }

  res.status(200).json({note: {
    _id,
    userId: note.userId,
    completed: note.completed,
    text: note.text,
    createdDate: note.createdDate,
  }});
};

module.exports.updateNote = async (req, res) => {
  const _id = req.params.id;
  const text = req.body.text;

  if (!text) {
    return res.status(400).json({message: 'Enter valid text'});
  }
  if (!(await Note.findByIdAndUpdate(_id, {text}))) {
    return res.status(400).json({message: 'Note was not found'});
  }

  res.status(200).json({message: 'Success'});
};

module.exports.checkNote = async (req, res) => {
  const _id = req.params.id;

  const note = await Note.findById(_id);
  if (!note) {
    return res.status(400).json({message: 'Note was not found'});
  }
  note.completed = !note.completed;
  await note.save();

  res.status(200).json({message: 'Success'});
};

module.exports.deleteNote = async (req, res) => {
  const _id = req.params.id;

  if (!(await Note.findByIdAndDelete(_id))) {
    return res.status(400).json({message: 'Note was not found'});
  }

  res.status(200).json({message: 'Success'});
};
