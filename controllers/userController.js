const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

module.exports.getInfo = async (req, res) => {
  const {username, _id} = req.user;

  const user = await User.findOne({username});
  if (!user) {
    return res.status(400).json({message: 'User was not found'});
  }
  res.status(200).json({user: {
    _id,
    username: user.username,
    createdDate: user.createdDate,
  },
  });
};

module.exports.deleteUser = async (req, res) => {
  const username = req.user.username;

  if ( !(await User.findOneAndDelete({username})) ) {
    return res.status(400).json({message: 'User was not found'});
  };
  res.status(200).json({message: 'Success'});
};

module.exports.changePassword = async (req, res) => {
  const {newPassword, oldPassword} = req.body;
  const username = req.user.username;
  const user = await User.findOne({username});

  if (!user) {
    res.status(400).json({message: 'User was not found'});
  }
  if ( !(await bcrypt.compare(oldPassword, user.password)) ) {
    return res.status(400).json({message: `Wrong password!`});
  }

  const newPasswordHash = await bcrypt.hash(newPassword, 10);
  const query = {'password': user.password};
  await User.findOneAndUpdate(query, {password: newPasswordHash});
  res.status(200).json({message: 'Success'});
};

