require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();
const {PORT} = require('./config');

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const notesRouter = require('./routers/notesRouter');

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/notes', notesRouter);


app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});

const start = async () => {
  await mongoose.connect('mongodb+srv://testuser:testuser@cluster0.pqsd5.mongodb.net/test', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(PORT, () => {
    console.log(`Server is running at ${PORT} port`);
  });
};

start();

