const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema({

  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    required: true,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
},
{
  versionKey: false,
});

module.exports.Note = mongoose.model('Note', noteSchema);
